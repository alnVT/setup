syntax enable
colorscheme ron "bre
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab autoindent smartindent cindent
set number
set showcmd
"set cursorline
filetype indent on
"set lazyredraw
"set wildmenu
set showmatch
set incsearch hlsearch
set foldenable
set foldmethod=syntax
set foldlevelstart=10
set foldnestmax=10
"set keymap=dvorak
"set mouse+=a